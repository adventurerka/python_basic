""" Модуль с юнит тестами моделей
"""
import unittest
from datetime import datetime
from random import randint

from lesson8.exeptions import (
    BulletsOffException, DiscountTypeException, DiscountValueException,
    AirbagException, BulletsTypeException, ItemException, ItemNameException,
    ItemValueException, EmployeeIdException, BonusTypeException,
)
from lesson8.models import PassengerCar, WarShip, BrokenCar, Ship, Chair


class TestModels(unittest.TestCase):
    """ Класс с юнит тестами моделей
    """

    def test_get_sound(self):
        """ Проверка метода издать звук, для корабля, военного корабля
        и пассажирского автомобиля
        """
        for cls in (PassengerCar, WarShip, Ship):
            self.assertEqual(cls.sound, cls().get_sound())

    def test_shoot_warship(self):
        """ Проверка функционала стрельбы для военного корабля
        """
        bullets = randint(1, 10)
        warship = WarShip(bullets=bullets)
        warship.add_bullets(bullets)
        self.assertEqual(bullets * 2, warship.bullets)
        warship.shoot(bullets * 2 - 1)
        warship.shoot()

    def test_shoot_warship_negative(self):
        """ Проверка негативных кейсов функционала стрельбы
        для военного корабля
        """
        warship = WarShip()
        self.assertRaises(BulletsTypeException, warship.shoot, '1')
        self.assertRaises(BulletsTypeException, warship.shoot, -1)
        self.assertRaises(BulletsOffException, warship.shoot)
        self.assertRaises(BulletsTypeException, warship.add_bullets, '1')
        self.assertRaises(BulletsTypeException, warship.add_bullets, -1)

    def test_rent_warship(self):
        """ Проверка метода расчета месячной аренды легкового военного корабля
        """
        self.assertEqual(
            5.0 if datetime.now().month == 12 else 10,
            WarShip(month_rent=10).rent
        )

    def test_rent_ship(self):
        """ Проверка метода расчета месячной аренды легкового корабля
        """
        self.assertEqual(1.5, Ship(month_rent=1.5).rent)

    def test_broken_car(self):
        """ Создать экземпляр класса без реализации абстрактного метода
        """
        self.assertRaises(TypeError, BrokenCar)

    def test_airbag_passenger_car(self):
        """ Проверка функционала раскрытия подушки безопасности для
        легкового автомобиля
        """
        passenger_car = PassengerCar()
        passenger_car.open_airbag()
        self.assertRaises(AirbagException, passenger_car.open_airbag)
        passenger_car.refresh_airbag()
        passenger_car.open_airbag()

    def test_get_price_passenger_car(self):
        """ Проверка метода расчета стоимости легкового автомобиля
        """
        self.assertEqual(9, PassengerCar(base_price=10).get_price(1))

    def test_get_price_passenger_car_negative(self):
        """ Проверка негативных кейсов метода расчета стоимости
        легкового автомобиля
        """
        passenger_car = PassengerCar()
        self.assertRaises(DiscountTypeException, passenger_car.get_price, '1')
        self.assertRaises(DiscountTypeException, passenger_car.get_price, -1)
        self.assertRaises(DiscountValueException, passenger_car.get_price, 1)

    def test_storage_negative(self):
        """ Негативные кейсы функционала загрузки транспорта
        """
        for cls in (PassengerCar, WarShip, Ship):
            obj = cls()
            self.assertRaises(ItemException, obj.remove, 'cat')
            obj.fill('cat', 1)
            self.assertRaises(ItemNameException, obj.fill, 'cat', 1)
            self.assertRaises(ItemNameException, obj.fill, 1, 1)
            self.assertRaises(ItemValueException, obj.fill, 'dog', 'dog')
            self.assertRaises(ItemValueException, obj.fill, 'dog', -1)

    def test_storage(self):
        """ Проверить функционал загрузки транспорта
        """
        for cls in (PassengerCar, WarShip, Ship):
            obj = cls()
            obj.fill('cat', 1)
            obj.fill('dog', 2)
            obj.remove('dog')
            self.assertEqual({'cat': 1}, obj.storage)

    def test_employee_warship(self):
        """ Проверить функционала сотрудников военного корабля
        """
        warship = WarShip()
        id_1 = warship.add_employee('Иван', 'Иванов', 10)
        self.assertEqual(11, warship.get_employee_salary(id_1, 1))
        warship.remove_employee(id_1)

    def test_employee_warship_negative(self):
        """ Негативные кейсы функционала сотрудников военного корабля
        """
        warship = WarShip()
        self.assertRaises(EmployeeIdException, warship.remove_employee, '1')
        self.assertRaises(
            EmployeeIdException, warship.get_employee_salary, '1'
        )
        id_1 = warship.add_employee('Иван', 'Иванов', 10)
        self.assertRaises(
            BonusTypeException, warship.get_employee_salary, id_1, '1'
        )
        self.assertRaises(
            BonusTypeException, warship.get_employee_salary, id_1, -1
        )

    def test_chairs_passenger_car(self):
        """ Проверить функционала установки кресел легкового автомобиля
        """
        passenger_car = PassengerCar(chair=Chair(color='red', age=5))
        passenger_car.reset_chairs()
        for chair in passenger_car.chairs:
            self.assertEqual(0, chair.age)
            self.assertEqual('red', chair.color)
            self.assertEqual('leather', chair.fabric)


if __name__ == '__main__':
    unittest.main()
