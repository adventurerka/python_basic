""" Модуль модели корабля
"""
from lesson8.models.vehicle import Vehicle


class Ship(Vehicle):
    """ Корабль
    """
    sound = 'Звук корабля'

    def __init__(self, month_rent: float = 0):
        super().__init__()
        self._month_rent = month_rent

    @property
    def rent(self) -> float:
        """ Месячная стоимость аренды
        :return стоимость
        """
        return self._month_rent
