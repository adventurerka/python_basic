""" Модуль модели легкового автомобиля
"""
from dataclasses import dataclass

from lesson8.exeptions import (
    DiscountTypeException, DiscountValueException, AirbagException,
)
from lesson8.models.car import Car


@dataclass
class Chair:
    """ Кресло
    """
    color: str = 'black'
    fabric: str = 'leather'
    age: int = 0

    def reset_chair(self):
        """ Установить новое кресло
        """
        self.age = 0


class PassengerCar(Car):
    """ Легковой автомобиль
    """

    def __init__(self, base_price: int = 0, chair: Chair = Chair()):
        super().__init__(base_price)
        self._is_airbag_available = True
        self._chair_amount = 4
        self._chairs = [chair for _ in range(self._chair_amount)]

    def open_airbag(self):
        """ Использование подушек безопасности
        """
        if not self._is_airbag_available:
            raise AirbagException
        self._is_airbag_available = False

    def refresh_airbag(self):
        """ Установить новые подушки безопасности
        """
        self._is_airbag_available = True

    def get_price(self, discount: int) -> int:
        """ Стоимость автомобиля
        :param discount: скидка
        :return стоимость
        """
        if not isinstance(discount, int) or discount < 0:
            raise DiscountTypeException
        if self._base_price < discount:
            raise DiscountValueException
        return self._base_price - discount

    def reset_chairs(self):
        """ Установить новые кресла
        """
        for chair in self._chairs:
            chair.reset_chair()

    @property
    def chairs(self) -> list:
        """ Кресла
        :return: список кресел
        """
        return self._chairs
