""" Модуль моделей
"""
from .broken_car import BrokenCar
from .passenger_car import PassengerCar, Chair
from .ship import Ship
from .warship import WarShip
