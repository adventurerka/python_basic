""" Модуль модели автомобиля
"""
from abc import ABC, abstractmethod

from lesson8.models.vehicle import Vehicle


class Car(Vehicle, ABC):
    """ Автомобиль
    """
    sound = 'Звук автомобиля'

    def __init__(self, base_price: int = 0):
        super().__init__()
        self._base_price = base_price

    @abstractmethod
    def get_price(self, discount: int) -> int:
        """ Стоимость автомобиля
        :param discount: скидка
        """
