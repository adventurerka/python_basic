""" Модуль модели боевого корабля
"""
import uuid
from dataclasses import dataclass
from datetime import datetime
from typing import Optional

from lesson8.exeptions import (
    BulletsOffException, BulletsTypeException, EmployeeIdException,
    BonusTypeException,
)
from lesson8.models.ship import Ship


@dataclass
class Employee:
    """ Работник
    """
    _id: str
    _first_name: str
    _last_name: str
    _base_salary: int
    _middle_name: Optional[str]

    def get_salary(self, bonus: int = 0) -> int:
        """ Получить размер заработной платы
        """
        if not isinstance(bonus, int) or bonus < 0:
            raise BonusTypeException
        return self._base_salary + bonus


class WarShip(Ship):
    """ Боевой корабль
    """

    def __init__(self, month_rent: float = 0, bullets:  int = 0):
        super().__init__(month_rent)
        self._bullets = bullets
        self._employees = dict()

    def shoot(self, amount: int = 1):
        """ Выстрелить
        :param amount: колличестово выстрелов
        """
        if not isinstance(amount, int) or amount < 0:
            raise BulletsTypeException
        if self._bullets < amount:
            raise BulletsOffException
        self._bullets -= amount

    def add_bullets(self, amount: int):
        """ Добавить патроны
        :param amount: колличестово зарядов
        """
        if not isinstance(amount, int) or amount < 0:
            raise BulletsTypeException
        self._bullets += amount

    @property
    def bullets(self) -> int:
        """ Колличество зарядов
        :return колличестово зарядов
        """
        return self._bullets

    @property
    def rent(self) -> float:
        """ Месячная стоимость аренды
        :return стоимость
        """
        if datetime.now().month == 12:
            # Рождественская скидка
            return super().rent * 0.5
        return super().rent

    def add_employee(
            self, first_name: str, last_name: str,
            base_salary: int, middle_name: str = None,
    ) -> str:
        """ Добавить сотрудника
        :param first_name имя
        :param last_name фамилия
        :param base_salary ставка заработной платы
        :param middle_name отчество
        :return идентификатор сотрудника
        """
        id_ = str(uuid.uuid4())
        self._employees[id_] = Employee(
            id_, first_name, last_name, base_salary, middle_name
        )
        return id_

    # pylint: disable=raise-missing-from
    def remove_employee(self, id_: str):
        """ Убрать сотрудника
        :param id_ идентификатор сотрудника
        """
        try:
            self._employees.pop(id_)
        except KeyError:
            raise EmployeeIdException

    # pylint: disable=raise-missing-from
    def get_employee_salary(self, id_: str, bonus: int = 0) -> int:
        """ Получить заработную плату сотрудника
        :param id_ идентификатор сотрудника
        :param bonus премия
        :return заработная плата
        """
        try:
            return self._employees[id_].get_salary(bonus)
        except KeyError:
            raise EmployeeIdException
