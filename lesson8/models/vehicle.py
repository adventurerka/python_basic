""" Модуль модели средства передвижения
"""
from lesson8.exeptions import (
    ItemException, ItemValueException, ItemNameException,
)


class Vehicle:
    """ Средство передвижения
    """
    sound = None

    def __init__(self):
        self._storage = dict()

    def get_sound(self):
        """ Издать звук
        """
        return self.sound

    def fill(self, name: str, value: int):
        """ Добавить груз
        :param name: имя груза
        :param value: величина
        """
        if not isinstance(value, int) or value < 0:
            raise ItemValueException
        if not isinstance(name, str) or name in self._storage:
            raise ItemNameException
        self._storage[name] = value

    # pylint: disable=raise-missing-from
    def remove(self, name: str):
        """ Убрать груз
        :param name: имя груза
        """
        try:
            self._storage.pop(name)
        except KeyError:
            raise ItemException

    @property
    def storage(self) -> dict:
        """ Груз в хранилище
        :return: словарь груза в хранилище
        """
        return self._storage
