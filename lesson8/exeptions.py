""" Модуль исключений
"""


class BulletsOffException(Exception):
    """ Недостаточно зарядов
    """


class BulletsTypeException(Exception):
    """ Некорректный тип зарядов
    """


class AirbagException(Exception):
    """ Подушки безопасности уже использованы
    """


class DiscountValueException(Exception):
    """ Некорректное значение скидки
    """


class DiscountTypeException(Exception):
    """ Некорректный тип скидки
    """


class ItemException(Exception):
    """ Груз с таким названием отсутствует
    """


class ItemValueException(Exception):
    """ Некорректный тип значения груза
    """


class ItemNameException(Exception):
    """ Некорректный тип имени груза
    """


class EmployeeIdException(Exception):
    """ Некорректный идентификатор сотрудника
    """


class BonusTypeException(Exception):
    """ Некорректный тип премии
    """
