""" Модуль модели студента
"""
from sqlalchemy import Column, Integer, String

from models.database import db


class Student(db.Model):
    """ Модель студента
    """
    id = Column(Integer, primary_key=True)
    name = Column(
        String, unique=True, nullable=False, default='', server_default=''
    )
