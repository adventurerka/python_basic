""" Базовый модуль для моделей
"""
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
