""" Модуль с представлениями для студентов
"""
from flask import Blueprint, render_template, request, redirect, url_for
from werkzeug.exceptions import NotFound

from models.database import db
from models.student import Student

student_app = Blueprint('student_app', __name__)


@student_app.route('/', endpoint='list')
def students_list():
    """ Получить список студентов
    """
    return render_template(
        'students/index.html', students=Student.query.all()
    )


@student_app.route('/<int:student_id>/', endpoint='details')
def student_details(student_id: int):
    """ Детальная информация о студенте
    """
    student = Student.query.filter_by(id=student_id).one_or_none()
    if student is None:
        raise NotFound()

    return render_template(
        'students/details.html',
        student=student
    )


@student_app.route('/add/', methods=['GET', 'POST'], endpoint='add')
def student_add():
    """ Добавить студента
    """
    if request.method == 'GET':
        return render_template('students/add.html')
    student = Student(name=request.form.get("student-name"))
    db.session.add(student)
    db.session.commit()
    return redirect(url_for('student_app.list'))
