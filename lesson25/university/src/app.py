""" Модуль для запуска сервера
"""
from flask import Flask, render_template
from flask_migrate import Migrate

from blueprints.students import student_app
from models.database import db


app = Flask(__name__)
app.config.from_object('config.DevelopmentConfig')
db.init_app(app)
migrate = Migrate(app, db)
app.register_blueprint(student_app, url_prefix='/student')


@app.route('/')
def index_page():
    """ Домашная страница
    """
    return render_template('index.html')


if __name__ == '__main__':
    app.run()
