""" Модуль для работы с файлом БД
"""
import json

from settings import BD_FILE_NAME


def read_data() -> dict:
    """ Прочитать данные из файла БД
    """
    try:
        with open(BD_FILE_NAME, 'r') as obj:
            data = obj.read()
            if not data:
                return {}
            data = json.loads(data)
            data = dict(map(lambda item: (int(item[0]), item[1]), data.items()))
            return data
    except FileNotFoundError:
        with open(BD_FILE_NAME, 'w') as obj:
            obj.write(json.dumps({}))
            return {}


def save_data(data: dict):
    """ Сохранить данные в файл БД
    """
    with open(BD_FILE_NAME, 'w') as obj:
        obj.write(json.dumps(data))
