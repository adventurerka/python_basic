""" Модуль для запуска сервера
"""
from flask import Flask, render_template

from blueprints.students import student_app
from settings import PORT, HOST

app = Flask(__name__)
app.register_blueprint(student_app, url_prefix='/student')


@app.route('/')
def index_page():
    """ Домашная страница
    """
    return render_template('index.html')


if __name__ == '__main__':
    app.run(host=HOST, port=PORT, debug=True, )
