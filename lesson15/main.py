""" Модуль запуска проекта
"""
import asyncio

import aiohttp

from lesson15.config import BASE_URL, DB_BIND
from lesson15.models import User, Post, db


async def get(obj_type: str) -> list:
    """ Запросить
    :param obj_type: тип запрашиваемых объектов
    :return: список json объектов
    """
    async with aiohttp.ClientSession() as session:
        async with session.get(f'{BASE_URL}{obj_type}') as response:
            return await response.json()


async def get_users() -> list:
    """ Запросить пользователей
    :return: список json пользователей
    """
    return await get('users')


async def get_posts() -> list:
    """ Запросить посты
    :return: список json постов
    """
    return await get('posts')


async def save_posts():
    """ Записать в БД посты
    """
    result = await get_posts()
    for post in result:
        for _ in range(200):
            user = await User.get(post['userId'])
            if user is not None:
                await Post.create(**post)
                break
            await asyncio.sleep(0.01)


async def save_users():
    """ Записать в БД пользователей
    """
    result = await get_users()
    for user in result:
        await User.create(**user)


async def clean_tables():
    """ Очистить таблицы posts, users
    """
    await Post.delete.gino.status()
    await User.delete.gino.status()


async def save_data_in_db():
    """ Получить информацию и записать в БД
    """
    await db.set_bind(DB_BIND)
    await clean_tables()
    await asyncio.wait({
        asyncio.create_task(func())
        for func in (save_users, save_posts,)
    })


if __name__ == '__main__':
    asyncio.run(save_data_in_db())
