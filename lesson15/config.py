""" Модуль с настройками проекта
"""

DB_CREDS = {
    'user': 'admin',
    'password': 'password',
    'database': 'postgres',
    'host': '127.0.0.1'
}
DB_BIND = 'postgresql://{user}:{password}@{host}/{database}'.format(**DB_CREDS)
BASE_URL = 'https://jsonplaceholder.typicode.com/'
