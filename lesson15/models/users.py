""" Модуль модели пользователя
"""
from . import db


class User(db.Model):
    """ Модель пользователя
    """
    # pylint: disable=(no-member, too-few-public-methods,)
    __tablename__ = 'users'

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String())
    username = db.Column(db.String())
    email = db.Column(db.String())
    address = db.Column(db.JSON())
    phone = db.Column(db.String())
    website = db.Column(db.String())
    company = db.Column(db.JSON())
