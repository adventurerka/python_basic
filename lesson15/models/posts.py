""" Модуль модели поста
"""
from . import db


class Post(db.Model):
    """ Модель поста
    """
    # pylint: disable=(no-member, too-few-public-methods,)
    __tablename__ = 'posts'

    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String())
    body = db.Column(db.String())
    userId = db.Column(db.Integer(), db.ForeignKey('users.id'), nullable=False)
