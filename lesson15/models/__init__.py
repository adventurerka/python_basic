""" Модуль импорта моделей
"""
from .base import db
from .users import User
from .posts import Post
