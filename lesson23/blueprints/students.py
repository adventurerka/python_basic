""" Модуль с представлениями для студентов
"""
from flask import Blueprint, render_template, request, redirect, url_for
from werkzeug.exceptions import NotFound

from lesson23 import tools

student_app = Blueprint('student_app', __name__)


@student_app.route('/', endpoint='list')
def students_list():
    """ Получить список студентов
    """
    return render_template(
        'students/index.html', students=tools.read_data()
    )


@student_app.route('/<int:student_id>/', endpoint='details')
def student_details(student_id: int):
    """ Детальная информация о студенте
    """
    students = tools.read_data()
    if student_id not in students:
        raise NotFound()

    student_name = students[student_id]
    return render_template(
        'students/details.html',
        student_id=student_id,
        student_name=student_name
    )


@student_app.route('/add/', methods=['GET', 'POST'], endpoint='add')
def student_add():
    """ Добавить студента
    """
    if request.method == 'GET':
        return render_template('students/add.html')
    students = tools.read_data()
    students[len(students) + 1] = request.form.get('student-name')
    tools.save_data(students)
    return redirect(url_for('student_app.list'))
