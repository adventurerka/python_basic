import logging
import unittest
from datetime import datetime
from functools import wraps


def func_repr(func, *args, **kwargs) -> str:
    """ Строковое отображение имени функции и аргументов
    """
    return '{}({})'.format(func.__name__, ', '.join(
        [str(a) for a in args] +
        [f'{a}={b}' for a, b in kwargs.items()]
    ))


def benchmark(func):
    """ Логгирование времени выполнения функции
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        start = datetime.now()
        return_value = func(*args, **kwargs)
        delta = datetime.now() - start
        logging.warning(
            'Время выполнения %s: %02d:%06d секунд.',
            func_repr(func, *args, **kwargs), delta.seconds,
            delta.microseconds
        )
        return return_value

    return wrapper


def trace(func):
    """ Логгирование вызовов функции
    """
    _space = [0]

    @wraps(func)
    def wrapper(*args, **kwargs):
        space = '_' * _space[0]
        fr = func_repr(func, *args, **kwargs)
        logging.warning('%s --> %s', space, fr)
        _space[0] += 1
        return_value = func(*args, **kwargs)
        _space[0] -= 1
        logging.warning('%s <-- %s == %s', space, fr, return_value)
        return return_value

    return wrapper


EVEN = 'EVEN'
ODD = 'ODD'
PRIME = 'PRIME'


def is_prime_number(value: int) -> bool:
    """ Проверка является ли число value простым
    :param value: число
    :return: True если число простое, иначе False
    """
    if value > 1:
        for i in range(2, value):
            if value % i == 0:
                return False
    return True


def is_even(value: int) -> bool:
    """ Проверка числа value на четность
    :param value: число
    :return: True если число четное, иначе False
    """
    return value % 2 == 0


def is_odd(value: int) -> bool:
    """ Проверка числа value на нечетность
    :param value: число
    :return: True если число нечетное, иначе False
    """
    return value % 2 != 0


FILTER_MAP = {
    EVEN: is_even,
    ODD: is_odd,
    PRIME: is_prime_number,
}


@benchmark
def digit_list_filter(values: list, filter_type: str) -> list:
    """ Фильтрация по filter_type чисел в списке values
    :param values: список чисел
    :param filter_type: тип фильтра, возможные значения: EVEN, ODD, PRIME
    :return: новый список чисел
    """
    func = FILTER_MAP[filter_type]
    return list(filter(func, values))


@benchmark
def digit_list_pow(values: list, amount: int = 2) -> list:
    """ Возведение в степень amount чисел в списке values
    :param values: список чисел
    :param amount: степень
    :return: новый список чисел
    """
    return list(map(pow, values, [amount for _ in range(len(values))]))


@trace
def fibonacci(n: int) -> int:
    """ Вычисление n-го числа ряда Фибоначчи
    :param n: номер числа в ряду
    :return: n-ое число
    """
    if n == 0:
        return 0
    if n in (1, 2):
        return 1
    return fibonacci(n - 1) + fibonacci(n - 2)


class TestDigitFuncs(unittest.TestCase):

    def test_custom_pow(self):
        self.assertEqual([1, 8, 27], digit_list_pow([1, 2, 3], 3))

    def test_default_pow(self):
        self.assertEqual([0, 1, 4, 9], digit_list_pow([0, 1, 2, 3]))

    def test_even(self):
        self.assertEqual([0, 2, 4], digit_list_filter([0, 1, 2, 3, 4], EVEN))

    def test_odd(self):
        self.assertEqual([1, 3], digit_list_filter([0, 1, 2, 3, 4], ODD))

    def test_prime(self):
        self.assertEqual([0, 1, 11], digit_list_filter([0, 1, 9, 11], PRIME))

    def test_fibonacci(self):
        self.assertEqual(8, fibonacci(6))

    def test_fibonacci_for_zero(self):
        self.assertEqual(0, fibonacci(0))


if __name__ == '__main__':
    unittest.main()
